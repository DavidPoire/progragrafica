#include "Vec2.h"

Matriz Matriz::multMatriz(Matriz x, Matriz y)
{
	// [y][x]
	Matriz z;
	z.mTrix[0][0] = (x.mTrix[0][0] * y.mTrix[0][0]) + (x.mTrix[0][1] * y.mTrix[1][0]) + (x.mTrix[0][2] * y.mTrix[2][0]);
	z.mTrix[0][1] = (x.mTrix[0][0] * y.mTrix[0][1]) + (x.mTrix[0][1] * y.mTrix[1][1]) + (x.mTrix[0][2] * y.mTrix[2][1]);
	z.mTrix[0][2] = (x.mTrix[0][0] * y.mTrix[0][2]) + (x.mTrix[0][1] * y.mTrix[1][2]) + (x.mTrix[0][2] * y.mTrix[2][2]);

	z.mTrix[1][0] = (x.mTrix[1][0] * y.mTrix[0][0]) + (x.mTrix[1][1] * y.mTrix[1][0]) + (x.mTrix[1][2] * y.mTrix[2][0]);
	z.mTrix[1][1] = (x.mTrix[1][0] * y.mTrix[0][1]) + (x.mTrix[1][1] * y.mTrix[1][1]) + (x.mTrix[1][2] * y.mTrix[2][1]);
	z.mTrix[1][2] = (x.mTrix[1][0] * y.mTrix[0][2]) + (x.mTrix[1][1] * y.mTrix[1][2]) + (x.mTrix[1][2] * y.mTrix[2][2]);

	z.mTrix[2][0] = (x.mTrix[2][0] * y.mTrix[0][0]) + (x.mTrix[2][1] * y.mTrix[1][0]) + (x.mTrix[2][2] * y.mTrix[2][0]);
	z.mTrix[2][1] = (x.mTrix[2][0] * y.mTrix[0][1]) + (x.mTrix[2][1] * y.mTrix[1][1]) + (x.mTrix[2][2] * y.mTrix[2][1]);
	z.mTrix[2][2] = (x.mTrix[2][0] * y.mTrix[0][2]) + (x.mTrix[2][1] * y.mTrix[1][2]) + (x.mTrix[2][2] * y.mTrix[2][2]);
	return z;
}