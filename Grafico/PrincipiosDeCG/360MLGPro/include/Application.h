#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <iostream>
#include "Object3D.h"
#include "Plane.h"
#include <stack>
#include "GLFW\glfw3.h"
#include <vector>
#include "Vec2.h"






class Application {
public:

	static const int WIDTH = 512;
	static const int HEIGHT = 512;
	static const int RGB = 3;
	static const int RGBA = 4;

	Application();
	~Application();

	void init();
	void display();
	void reshape(int w, int h);
	void keyboard(int key, int scancode, int action, int mods);
	void update();
	void swapbuffers();
	void cursor_position(double xpos, double ypos);
	void putPixel(GLubyte *buffer, int x, int y, unsigned char R, unsigned char G, unsigned char B, unsigned char A);
	void putPixel(int x, int y, unsigned char R, unsigned char G, unsigned char B, unsigned char A);
	void draw();
	void putPixel(int x, int y);
	void setUp();
	int CreateHash(int dx, int dy);
	std::vector<Vec3> poligonos(int lados, int radio);

	
	void moveTo(int x, int y);
	void lineTo(int x, int y);
	void clearScreen();
	void circleMagic(int cX, int cY,int ratio);
	Vec3 puntoMedio(Vec3 x, Vec3 y);
	std::vector<std::vector<Vec3>> Blundelinsky(int levelu, int radio);
	
	

	std::stack<glm::mat4> mStack;
	glm::mat4 mProjectionMatrix, mTransform;
	glm::vec3 vEye;
	GLFWwindow* window;
	unsigned char Buffers[2][WIDTH*HEIGHT*RGBA];

private:	
	GLuint texturesID[2], pboID[2];
	GLuint shaderID;
	GLuint VAO, VBO;
	GLuint sampler;
	GLuint uTransform;
	GLubyte* _screenBuffer;
	int _currentBuffer, 
		_nextBuffer;

	glm::vec3 myLightPosition;
	GLuint uMyLightPosition[2];	

	Plane oPlane;
	float fTime;
	void initTextures();
	void processPBO();
	void initPBOs();
	void updatePixels(GLubyte *buffer);
	int _drawMode,
		shader;
	bool moveLight;
	void setColor(int R, int G, int B, int A);
	Vec3 multMatriz(Matriz x, Vec3 y);
	void fakeBuffers();
	int r, g, b, a, x0 = -100,y0 = -100;
	int tX, tY;
	int angulo = 0;
	Matriz zRotation, xTranslation, fullmovement;
	
	
};


#endif //__APPLICATION_H__