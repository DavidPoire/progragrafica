#include "Application.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <math.h>
#include "Vec2.h"




#define PI 3.1416

std::vector<std::vector<Vec3>> vertex;
void Application::update()
{

	
#pragma region zRotation

	zRotation.mTrix[0][0] = cos((angulo * PI)/180);
	zRotation.mTrix[0][1] = -sin((angulo * PI) / 180);
	zRotation.mTrix[0][2] = 0;

	zRotation.mTrix[1][0] = sin((angulo * PI) / 180);
	zRotation.mTrix[1][1] = cos((angulo * PI) / 180);
	zRotation.mTrix[1][2] = 0;

	zRotation.mTrix[2][0] = 0;
	zRotation.mTrix[2][1] = 0;
	zRotation.mTrix[2][2] = 1;
#pragma endregion

#pragma region xTranslation

	xTranslation.mTrix[0][0] = 1;
	xTranslation.mTrix[0][1] = 0;
	xTranslation.mTrix[0][2] = tX;

	xTranslation.mTrix[1][0] = 0;
	xTranslation.mTrix[1][1] = 1;
	xTranslation.mTrix[1][2] = tY;

	xTranslation.mTrix[2][0] = 0;
	xTranslation.mTrix[2][1] = 0;
	xTranslation.mTrix[2][2] = 1;

#pragma endregion
	fullmovement = fullmovement.multMatriz(xTranslation, zRotation);

	for (int i = 0; i < vertex.size(); ++i)
	{
		for (int j = 0; j < vertex.at(i).size(); ++j)
		{
			vertex.at(i).at(j) = multMatriz(fullmovement, vertex.at(i).at(j));
			
		}
	}
	
	angulo = (angulo +1) %360;
}

void Application::draw()
{
	clearScreen();
	setColor(175, 58, 255, 0);
	for (int i = 0; i < vertex.size(); ++i)
	{
		for (int j = 0; j < vertex.at(i).size(); ++j)
		{
			moveTo(vertex.at(i).at(j).v[0] + 200, vertex.at(i).at(j).v[1] + 200);
			lineTo(vertex.at(i).at((j + 1) % vertex.at(i).size()).v[0] + 200, vertex.at(i).at((j + 1) % vertex.at(i).size()).v[1] + 200);
		}
	}

	
}


void Application::setUp()
{

	vertex = Blundelinsky(4, 200);

	tX = 0;
	tY = 0;
	angulo = 0;

}
