#include "Application.h"
#include "Vec2.h"
#include <iostream>
#include <cstdlib>
#include <vector>


#define PI 3.1416

std::vector<Vec2> vertices;
void Application::update()
{


	

}

void Application::draw()
{
	setColor(255, 0, 255, 0);
	moveTo(255,255);
	for (int i = 0; i < vertices.size()-1; ++i)
		lineTo(vertices.at(i)._v[0], vertices.at(i)._v[1]);

	lineTo(255, 255);

	
	
}

void Application::putPixel(int x1, int y1)
{
	putPixel(x1, y1, r, g, b, a);
}

void Application::setUp()
{
	int lados;
	std::cin >> lados;
	poligonos(lados);
}

int Application::CreateHash(int dx, int dy)
{
	int ret = 0;
	dx >= 0 ? ++ret : --ret;
	dy >= 0 ? ret += 2 : ret -= 2;
	ret = abs(dx) >= abs(dy) ? ret << 1 : ret << 2;
	return ret;
} //Malagas DR

void Application::poligonos(int lados)
{
	int angulo = 0,
		incremento = 360 / lados;
	int radio=50;
	Vec2 miV;
	miV._v[0] = x0;
	miV._v[1] = y0;

	for (int i = lados+1;  --i;)
	{
		miV._v[0] += radio*cos(angulo * PI / 180);
		miV._v[1] += radio*sin(angulo * PI / 180);
		vertices.push_back(miV);
		angulo += incremento;

		

	}
}

void Application::setColor(int R, int G, int B, int A)
{
	r = R;
	g = G;
	b = B;
	a = A;
}

void Application::moveTo(int x1, int y1)
{
	x0 = x1;
	y0 = y1;
}



void Application::lineTo(int x1, int y1)
{
	int dy = y1 - y0,
		dx = x1 - x0,
		x = x0 + 1,//256 
		y = y0,
		caseHash = CreateHash(dx, dy);
	dx = abs(dx);
	dy = abs(dy);
	switch (caseHash)
	{
	case 6:
	{// dx > dy 
		int E = 2 * (dy),
			nE = 2 * (dy - dx),
			d = 2 * dy - dx;//0
		while (x != x1) {
			(d > 0) ? y++, d += nE : d += E;
			putPixel(x, y);
			++x;
		}
		break;
	}
	case -2: // dx > -dy
	{
		int E = 2 * (dy),
			nE = 2 * (dy - dx),
			d = 2 * dy - dx;//0
		while (x != x1) {
			(d > 0) ? y--, d += nE : d += E;
			putPixel(x, y);
			++x;
		}
		break;
	}

	case -4: // -dy > dx
	{
		int E = 2 * (dx),
			nE = 2 * (dx - dy),
			d = 2 * dx - dy;//0
		while (y != y1) {
			(d > 0) ? x++, d += nE : d += E;
			putPixel(x, y);
			--y;
		}
		break;
	}
	case -12: //-dy > -dx
	{
		int E = 2 * (dx),
			nE = 2 * (dx - dy),
			d = 2 * dx - dy;//0
		while (y != y1) {
			(d > 0) ? x--, d += nE : d += E;
			putPixel(x, y);
			--y;
		}
		break;
	}

	case -6: //-dx > -dy
	{
		int E = 2 * (dy),
			nE = 2 * (dy - dx),
			d = 2 * dy - dx;//0
		while (x != x1) {
			(d > 0) ? y--, d += nE : d += E;
			putPixel(x, y);
			--x;
		}
		break;
	}
	case 2: //-dx> dy
	{
		int E = 2 * (dy),
			nE = 2 * (dy - dx),
			d = 2 * dy - dx;//0
		while (x != x1) {
			(d > 0) ? y++, d += nE : d += E;
			putPixel(x, y);
			--x;
		}
		break;
	}
	case 4: //dy > -dx
	{
		int E = 2 * (dx),
			nE = 2 * (dx - dy),
			d = 2 * dx - dy;//0
		while (y != y1) {
			(d > 0) ? --x, d += nE : d += E;
			putPixel(x, y);
			++y;
		}
		break;
	}
	case 12: //dy > dx
	{
		int E = 2 * (dx),
			nE = 2 * (dx - dy),
			d = 2 * dx - dy;//0
		while (y != y1) {
			(d > 0) ? ++x, d += nE : d += E;
			putPixel(x, y);
			++y;
		}
		break;
	}
	}
	moveTo(x1, y1);
} // Malagas DR

void Application::clearScreen()
{
	for (int i = 0; i < HEIGHT; ++i)
		for (int j = 0; j < WIDTH; ++j)
		{
			putPixel(j, i, 0, 0, 0, 255);
		}
}

void Application::circleMagic(int cx1, int cy1, int ratio)
{
	setColor(rand() % 255, rand() % 255, rand() % 255, 0);

	int y1 = 0, x1 = ratio, dE = (2 * x1) + 3, dSE = 2 * (x1 - y1) + 5;
	int d = 1 - ratio;
	int dx1 = 1, dy1 = 1;
	int z = dx1 - (ratio << 1);


	while (x1 >= y1)
	{
		putPixel(x1 + cx1, y1 + cy1); //0
		putPixel(y1 + cx1, -x1 + cy1); //1
		putPixel(-x1 + cx1, -y1 + cy1); //2
		putPixel(-y1 + cx1, x1 + cy1); //3
		putPixel(y1 + cx1, x1 + cy1); //4
		putPixel(-x1 + cx1, y1 + cy1); //5
		putPixel(-y1 + cx1, -x1 + cy1); //6
		putPixel(x1 + cx1, -y1 + cy1); //7



								   /*if (d < 0)
								   {
								   d += dE;
								   }
								   else
								   {
								   d += dSE;
								   --y1;
								   }
								   ++x1;*/

		if (z <= 0)
		{
			++y1;
			z += dy1;
			dy1 += 2;
		}
		else if (z > 0)
		{
			--x1;
			dx1 += 2;
			z += (-ratio << 1) + dx1;
		}



	}

}