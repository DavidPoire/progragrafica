#pragma once

class Vec2 
{	
public:

	float _v[2];
private:


};

class Vec3
{
public:
	float v[3];
};
class Vec4 
{
public:
	Vec4();
	Vec4(int, int, int);
	float v[4];

};
class Matriz
{
public:
	float mTrix[3][3];
	Matriz multMatriz(Matriz x, Matriz y);

};
class Matrix4
{
public:
	float v[4][4];
	Matrix4();
	void RotZ(float angle);
	void RotX(float angle);
	void RotY(float angle);
	void tras(float x, float y, float z);
	static Matrix4 multMatr4(Matrix4 x, Matrix4 y);
	static Vec4 multMatr4(Matrix4 x, Vec4 y);
};