#include "Vec2.h"
#include <cmath>
#define PI 3.1416

Matriz Matriz::multMatriz(Matriz x, Matriz y)
{
	// [y][x]
	Matriz z;
	z.mTrix[0][0] = (x.mTrix[0][0] * y.mTrix[0][0]) + (x.mTrix[0][1] * y.mTrix[1][0]) + (x.mTrix[0][2] * y.mTrix[2][0]);
	z.mTrix[0][1] = (x.mTrix[0][0] * y.mTrix[0][1]) + (x.mTrix[0][1] * y.mTrix[1][1]) + (x.mTrix[0][2] * y.mTrix[2][1]);
	z.mTrix[0][2] = (x.mTrix[0][0] * y.mTrix[0][2]) + (x.mTrix[0][1] * y.mTrix[1][2]) + (x.mTrix[0][2] * y.mTrix[2][2]);

	z.mTrix[1][0] = (x.mTrix[1][0] * y.mTrix[0][0]) + (x.mTrix[1][1] * y.mTrix[1][0]) + (x.mTrix[1][2] * y.mTrix[2][0]);
	z.mTrix[1][1] = (x.mTrix[1][0] * y.mTrix[0][1]) + (x.mTrix[1][1] * y.mTrix[1][1]) + (x.mTrix[1][2] * y.mTrix[2][1]);
	z.mTrix[1][2] = (x.mTrix[1][0] * y.mTrix[0][2]) + (x.mTrix[1][1] * y.mTrix[1][2]) + (x.mTrix[1][2] * y.mTrix[2][2]);

	z.mTrix[2][0] = (x.mTrix[2][0] * y.mTrix[0][0]) + (x.mTrix[2][1] * y.mTrix[1][0]) + (x.mTrix[2][2] * y.mTrix[2][0]);
	z.mTrix[2][1] = (x.mTrix[2][0] * y.mTrix[0][1]) + (x.mTrix[2][1] * y.mTrix[1][1]) + (x.mTrix[2][2] * y.mTrix[2][1]);
	z.mTrix[2][2] = (x.mTrix[2][0] * y.mTrix[0][2]) + (x.mTrix[2][1] * y.mTrix[1][2]) + (x.mTrix[2][2] * y.mTrix[2][2]);
	return z;
}

Matrix4::Matrix4()
{
	v[0][0] = 1;
	v[0][1] = 0;
	v[0][2] = 0;
	v[0][3] = 0;
	v[1][0] = 0;
	v[1][1] = 1;
	v[1][2] = 0;
	v[1][3] = 0;
	v[2][0] = 0;
	v[2][1] = 0;
	v[2][2] = 1;
	v[2][3] = 0;
	v[3][0] = 0;
	v[3][1] = 0;
	v[3][2] = 0;
	v[3][3] = 1;
}

void Matrix4::RotZ(float angle)
{
	v[0][0] = cos((angle * PI) / 180);
	v[0][1] = -sin((angle * PI) / 180);

	v[1][0] = sin((angle * PI) / 180);
	v[1][1] = cos((angle * PI) / 180);

}

void Matrix4::RotX(float angle)
{
	v[1][1] = cos((angle + PI) / 180);
	v[1][2] = -sin((angle + PI) / 180);

	v[2][1] = sin((angle + PI) / 180);
	v[2][2] = cos((angle + PI) / 180);
}

void Matrix4::RotY(float angle)
{
	v[0][0] = cos((angle + PI) / 180);
	v[0][2] = sin((angle + PI) / 180);

	v[2][0] = -sin((angle + PI) / 180);
	v[2][2] = cos((angle + PI) / 180);
	
}

void Matrix4::tras(float x, float y, float z)
{
	v[0][3] = x;
	v[1][3] = y;
	v[2][3] = z;
}

Matrix4 Matrix4::multMatr4(Matrix4 x, Matrix4 y)
{
	Matrix4 z;
	z.v[0][0] = (x.v[0][0] * y.v[0][0]) + (x.v[0][1] * y.v[1][0]) + (x.v[0][2] * y.v[2][0]) + (x.v[0][3] * y.v[3][0]);
	z.v[0][1] = (x.v[0][0] * y.v[0][1]) + (x.v[0][1] * y.v[1][1]) + (x.v[0][2] * y.v[2][1]) + (x.v[0][3] * y.v[3][1]);
	z.v[0][2] = (x.v[0][0] * y.v[0][2]) + (x.v[0][1] * y.v[1][2]) + (x.v[0][2] * y.v[2][2]) + (x.v[0][3] * y.v[3][2]);
	z.v[0][3] = (x.v[0][0] * y.v[0][3]) + (x.v[0][1] * y.v[1][3]) + (x.v[0][2] * y.v[2][3]) + (x.v[0][3] * y.v[3][3]);

	z.v[1][0] = (x.v[1][0] * y.v[0][0]) + (x.v[1][1] * y.v[1][0]) + (x.v[1][2] * y.v[2][0]) + (x.v[1][3] * y.v[3][0]);
	z.v[1][1] = (x.v[1][0] * y.v[0][1]) + (x.v[1][1] * y.v[1][1]) + (x.v[1][2] * y.v[2][1]) + (x.v[1][3] * y.v[3][1]);
	z.v[1][2] = (x.v[1][0] * y.v[0][2]) + (x.v[1][1] * y.v[1][2]) + (x.v[1][2] * y.v[2][2]) + (x.v[1][3] * y.v[3][2]);
	z.v[1][3] = (x.v[1][0] * y.v[0][3]) + (x.v[1][1] * y.v[1][3]) + (x.v[1][2] * y.v[2][3]) + (x.v[1][3] * y.v[3][3]);

	z.v[2][0] = (x.v[2][0] * y.v[0][0]) + (x.v[2][1] * y.v[1][0]) + (x.v[2][2] * y.v[2][0]) + (x.v[2][3] * y.v[3][0]);
	z.v[2][1] = (x.v[2][0] * y.v[0][1]) + (x.v[2][1] * y.v[1][1]) + (x.v[2][2] * y.v[2][1]) + (x.v[2][3] * y.v[3][1]);
	z.v[2][2] = (x.v[2][0] * y.v[0][2]) + (x.v[2][1] * y.v[1][2]) + (x.v[2][2] * y.v[2][2]) + (x.v[2][3] * y.v[3][2]);
	z.v[2][2] = (x.v[2][0] * y.v[0][3]) + (x.v[2][1] * y.v[1][3]) + (x.v[2][2] * y.v[2][3]) + (x.v[2][3] * y.v[3][3]);

	z.v[3][0] = (x.v[3][0] * y.v[0][0]) + (x.v[3][1] * y.v[1][0]) + (x.v[3][2] * y.v[2][0]) + (x.v[3][3] * y.v[3][0]);
	z.v[3][1] = (x.v[3][0] * y.v[0][1]) + (x.v[3][1] * y.v[1][1]) + (x.v[3][2] * y.v[2][1]) + (x.v[3][3] * y.v[3][1]);
	z.v[3][2] = (x.v[3][0] * y.v[0][2]) + (x.v[3][1] * y.v[1][2]) + (x.v[3][2] * y.v[2][2]) + (x.v[3][3] * y.v[3][2]);
	z.v[3][2] = (x.v[3][0] * y.v[0][3]) + (x.v[3][1] * y.v[1][3]) + (x.v[3][2] * y.v[2][3]) + (x.v[3][3] * y.v[3][3]);
	return z;
	
}

Vec4 Matrix4::multMatr4(Matrix4 x, Vec4 y)
{
	Vec4 z;
	z.v[0] = (x.v[0][0] * y.v[0]) + (x.v[0][1] * y.v[1]) + (x.v[0][2] * y.v[2]) + (x.v[0][3] * y.v[3]);
	z.v[1] = (x.v[1][0] * y.v[0]) + (x.v[1][1] * y.v[1]) + (x.v[1][2] * y.v[2]) + (x.v[1][3] * y.v[3]);
	z.v[2] = (x.v[2][0] * y.v[0]) + (x.v[2][1] * y.v[1]) + (x.v[2][2] * y.v[2]) + (x.v[2][3] * y.v[3]);
	z.v[3] = (x.v[3][0] * y.v[0]) + (x.v[3][1] * y.v[1]) + (x.v[3][2] * y.v[2]) + (x.v[3][3] * y.v[3]);
	return z;
}

Vec4::Vec4()
{
}

Vec4::Vec4(int x, int y, int z)
{
	this->v[0] = x;
	this->v[1] = y;
	this->v[2] = z;
	this->v[3] = 1;
}
