#include "Application.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <math.h>
#include "Vec2.h"



std::vector<Vec4> vecty, vecta;


void Application::setUp()
{

	vecty.push_back(Vec4(150, 200, 200)); //1
	vecty.push_back(Vec4(200, 200, 200)); //2
	vecty.push_back(Vec4(150, 150, 200)); //3
	vecty.push_back(Vec4(200, 150, 200)); //4
	vecty.push_back(Vec4(150, 200, 150)); //5
	vecty.push_back(Vec4(200, 200, 150)); //6
	vecty.push_back(Vec4(150, 150, 150)); //7
	vecty.push_back(Vec4(200, 150, 150)); //8

}

float ang = 0;
void Application::update()
{
	Matrix4 rot;
	Matrix4 scale;
	Matrix4 tras;
	Matrix4 fin;
	scale.v[0][0] = 100;
	scale.v[1][1] = 100;
	scale.v[2][2] = 100;
	rot.RotZ(ang += .1f);
	tras.tras(0, 0, 0);
	fin = Matrix4::multMatr4(tras, scale);
	fin = Matrix4::multMatr4(rot, fin);
	tras.tras(.5f, .5f, 2);
	fin = Matrix4::multMatr4(tras, fin);
	vecta.clear();
	for (auto i : vecty)
	{
		i.v[0] /= i.v[2];
		i.v[1] /= i.v[2];
		i = Matrix4::multMatr4(fin, i);
		vecta.push_back(i);
	}

}

void Application::draw()
{
	clearScreen();
	setColor(175, 58, 255, 0);
	for (int i = 0; i <= vecta.size() - 3; ++i)
	{
		moveTo(vecta.at(i).v[0], vecta.at(i).v[1]);
		lineTo(vecta.at(i + 1).v[0], vecta.at(i + 1).v[1]);
		lineTo(vecta.at(i + 2).v[0], vecta.at(i + 2).v[1]);
		lineTo(vecta.at(i).v[0], vecta.at(i).v[1]);


	}



}


