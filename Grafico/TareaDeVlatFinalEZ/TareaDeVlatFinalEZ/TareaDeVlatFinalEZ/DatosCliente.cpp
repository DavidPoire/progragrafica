#include "DatosCliente.h"
#include <string>

DatosCliente::DatosCliente(int x, std::string nom, std::string apell, double y)
{
	establecerNumCuenta(x);
	establecerApellidoPaterno(apell);
	establecerPrimerNombre(nom);
	establecerSaldo(y);
}

void DatosCliente::establecerNumCuenta(int x)
{
	numeroCuenta = x;
}

int DatosCliente::obtenerNumero() const
{
	return numeroCuenta;
}

void DatosCliente::establecerApellidoPaterno(std::string x)
{
	int longitud = x.size();
	longitud = (longitud <= 15 ? longitud : 14);
	for (int i = 0; i < longitud; ++i)
		apellidoPaterno[i] = x.at(i);
	apellidoPaterno[longitud] = '\0';
}

std::string DatosCliente::obtenerApellidoPaterno() const
{
	return apellidoPaterno;
}

void DatosCliente::establecerPrimerNombre(std::string x)
{
	
	int longitud = x.size();
	longitud = (longitud <= 10 ? longitud : 9);
	for (int i = 0; i < longitud; ++i)
		primerNombre[i] = x.at(i);
	primerNombre[longitud] = '\0';
}

std::string DatosCliente::obtenerPrimerNombre() const
{
	return primerNombre;
}

void DatosCliente::establecerSaldo(double x)
{
	saldo = x;
}

double DatosCliente::obtenerDato() const
{
	return saldo;
}
