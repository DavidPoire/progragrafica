#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <limits>
#include <iomanip>
#include "DatosCliente.h"
using std:: cerr;
using std:: setw;
using std:: cout;
using std:: endl;
using std:: cin;
using std:: left;
using std:: showpoint;
using std:: fixed;
using std:: right;
using std:: ios;
using std:: ofstream;
using std:: exit;
using std:: fstream;
using std:: ifstream;
using std:: ostream;
using std:: setprecision;


void creacionAA()
{
	std::ofstream CreditoSalida("Credito.dat", std::ios::out | std::ios::binary);

	if (!CreditoSalida)
	{
		std::cerr << "No se puedo abrir el archivo" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Le cambias el nombre a ClientData
	DatosCliente ClienteEnBlanco(0, "'\0'", "'\0'", 0);

	for (int i = 0; i < 100; i++)
	{
		CreditoSalida.write(reinterpret_cast<const char*>(&ClienteEnBlanco), sizeof(DatosCliente));
	}
}

bool checarPalabra(char x[])
{
	int size = strlen(x);

	for (int i = 0; i < size; ++i)
	{
		if (x[i] < 65 || x[i] > 90)
		{
			//No es Mayuscula
			if (x[i] < 97 || x[i] > 122)
			{

					cout << "El nombre o apellido fue mal escrito ";
					return true;
			}
		}
	}
	return false;
}
bool checarSaldin(std::string x)
{
	for (int i = 0; i < x.size(); ++i)
	{
		if (x.at(i) < 48 || x.at(i) > 57)
		{
			cout << "Error del Saldo";
			return true;
		}
	}
	return false;
}


void Escritura()
{
	int NumeroCuenta = 0;
	char ApellidoPaterno[15];
	char PrimerNombre[10];
	double Saldo = 0;

	std::fstream CreditoSalida("Credito.dat", std::ios::in | std::ios::out | std::ios::binary);

	if (!CreditoSalida)
	{
		std::cerr << "No se puedo abrir el archivo" << std::endl;
		exit(EXIT_FAILURE);
	}
	// Le cambias el nombre a ClientData
	DatosCliente Cliente(0, "'\0'", "'\0'", 0);

	std::cout << "Escriba el numero de cuenta (de 1 a 100, 0 para termninar la entrada)\n";
	std::cin >> NumeroCuenta;



	while (NumeroCuenta > 0 && NumeroCuenta <= 100)
	{
		std::string saldin;
		std::cout << "Introduzca apellido paterno, primer nombre y saldo\n";
		std::cin >> std::setw(15) >> ApellidoPaterno;
		if (checarPalabra(ApellidoPaterno) == true)
		{
			cout << "Por fabor escriba bien su Apellido\n";
			break;
		}
		else if (strlen(ApellidoPaterno) > 14) 
		{
			cout << "Tu apellido es muy largo... No nos culpes a nosotros, culpa a tus padres por no quererte. \n";
			break;
		}
		std::cin >> std::setw(10) >> PrimerNombre;
		if (checarPalabra(PrimerNombre) == true)
		{
			cout << "Por fabor escriba bien su Nombre\n";
			break;
		}
		else if (strlen(PrimerNombre) > 10)
		{
			cout << "Tu Nombre es muy largo... No nos culpes a nosotros, culpa a tus padres por no quererte. \n";
			break;
		}
		while (!(cin >> Saldo))
		{
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

			cout << "Please input a proper 'whole' number: " << endl;
		}
		
		
		


		Cliente.establecerNumCuenta(NumeroCuenta);
		Cliente.establecerApellidoPaterno(ApellidoPaterno);
		Cliente.establecerPrimerNombre(PrimerNombre);
		Cliente.establecerSaldo(Saldo);

		CreditoSalida.seekp((Cliente.obtenerNumero() - 1) * sizeof(DatosCliente));
		CreditoSalida.write(reinterpret_cast<const char*>(&Cliente), sizeof(DatosCliente));

		std::cout << "Introduzca numero de cuenta\n";
		std::cin >> NumeroCuenta;
	}
	
}

void Eliminar()
{
	int NumeroCuenta = 0;
	char ApellidoPaterno[15];
	char PrimerNombre[10];
	double Saldo = 0;

	std::fstream CreditoSalida("Credito.dat", std::ios::in | std::ios::out | std::ios::binary);

	if (!CreditoSalida)
	{
		std::cerr << "No se puedo abrir el archivo" << std::endl;
		exit(EXIT_FAILURE);
	}
	// Le cambias el nombre a ClientData
	DatosCliente Cliente(0, "'\0'", "'\0'", 0);

	std::cout << "Escriba el numero de cuenta (de 1 a 100, 0 para termninar la entrada)\n";
	std::cin >> NumeroCuenta;

	if (NumeroCuenta > 0 && NumeroCuenta <= 100)
	{
		Cliente.establecerNumCuenta(NumeroCuenta);
		Cliente.establecerApellidoPaterno(" ");
		Cliente.establecerPrimerNombre(" ");
		Cliente.establecerSaldo(NULL);
		

		CreditoSalida.seekp((Cliente.obtenerNumero() - 1) * sizeof(DatosCliente));
		CreditoSalida.write(reinterpret_cast<const char*>(&Cliente), sizeof(DatosCliente));
	}
	
	else
	{
		cout << "escriba un numero de cuenta valido\n";
	}


}

void imprimirLinea(ostream &salida, const DatosCliente &registro)
{
	salida << left << setw(10) << registro.obtenerNumero() <<
		setw(16) << registro.obtenerApellidoPaterno() <<
		setw(11) << registro.obtenerPrimerNombre() <<
		setw(10) << setprecision(2) << right << fixed <<
		showpoint << registro.obtenerDato() << endl;
}

void Lectura()
{
	std::ifstream CreditoEntrada("Credito.dat", std::ios::in | std::ios::binary);

	if (!CreditoEntrada)
	{
		std::cerr << "No se puedo abrir el archivo" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::cout << std::left << std::setw(10) << "Cuenta" << std::setw(16) << "Apellido" << std::setw(11) << "Nombre" <<
		std::left << std::setw(10) << std::right << "Saldo" << std::endl;

	// ClienData es el nombre que yo le puse a la clase, cambiaselo al que tu le pusiste
	DatosCliente Cliente(0, "'\0'", "'\0'", 0);

	CreditoEntrada.read(reinterpret_cast<char*>(&Cliente), sizeof(DatosCliente));

	while (CreditoEntrada && !CreditoEntrada.eof())
	{
		if (Cliente.obtenerNumero() != 0) { imprimirLinea(std::cout, Cliente); }

		CreditoEntrada.read(reinterpret_cast<char*>(&Cliente), sizeof(DatosCliente));
	}

}


int main()
{
	//creacionAA();
	std::string x;
	while (true)
	{
		
		cout << endl;
		cout << "Escriba que desea realizar: Crear, Leer, Actualizar , Eliminar, Salir . \n";
		cin >> x;

		if (x == "Crear")
		{
			Escritura();
		}
		else if(x == "Leer")
		{
			Lectura();
		}
		else if (x == "Actualizar")
		{
			Escritura();
		}
		else if (x == "Eliminar")
		{
			Eliminar();
		}
		else if (x == "Salir")
		{
			break;
		}
		else
		{
			cout << "Por favor escriba bien la accion que desea realizar. \n";
		}
	}

	system("pause");

	return 0;
}
