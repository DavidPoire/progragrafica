#pragma once
#include <string>

class DatosCliente
{
public:
	DatosCliente(int = 0, std::string = "" ,std::string = "", double = 0.0);

	void establecerNumCuenta(int x);
	int obtenerNumero() const;

	void establecerApellidoPaterno(std::string x);
	std::string obtenerApellidoPaterno() const;

	void establecerPrimerNombre(std::string x);
	std::string obtenerPrimerNombre() const;

	void establecerSaldo(double x);
	double obtenerDato()const;

private:
	int numeroCuenta;
	char apellidoPaterno[15];
	char primerNombre[10];
	double saldo;


};